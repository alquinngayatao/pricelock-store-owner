package com.pricelock.storeowner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Login extends AppCompatActivity {

    TextView forgotPasswordText;
    TextView registerText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        forgotPasswordText = (TextView) findViewById(R.id.textViewForgotPassword);
        forgotPasswordText.setOnClickListener(view -> {
            forgotPassword();
        });
        registerText = (TextView) findViewById(R.id.textViewRegister);
        registerText.setOnClickListener(view -> {
            register();
        });
    }

    public void forgotPassword(){
        Intent intent = new Intent(this, ForgotPassword.class);
        startActivity(intent);
    }

    public void register(){
        Intent intent = new Intent(this, Registration.class);
        startActivity(intent);
    }
}