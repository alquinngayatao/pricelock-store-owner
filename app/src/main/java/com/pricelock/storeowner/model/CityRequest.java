package com.pricelock.storeowner.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CityRequest implements Serializable {
    private String provinceCode;
}
