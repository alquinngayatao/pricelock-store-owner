package com.pricelock.storeowner.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StoreOwnerRegisterRequest implements Serializable {
    private String emailAddress;
    private String username;
    private String password;
    private String phoneNumber;
    private String storeName;
    private String province;
    private String city;
    private String barangay;
    private String streetAddress;
    private String validId;
    private String barangayBusinessPermit;
    private String dtiRegistration;
    private String mayorPermit;
}
