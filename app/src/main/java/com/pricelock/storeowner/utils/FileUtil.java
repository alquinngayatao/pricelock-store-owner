package com.pricelock.storeowner.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class FileUtil {
    public static String convertToBase64(String filePath){
        File documentFile = new File(filePath);
        if (documentFile.exists() && documentFile.length() > 0) {
            Bitmap bm = BitmapFactory.decodeFile(filePath);
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, bOut);
            return Base64.encodeToString(bOut.toByteArray(), Base64.DEFAULT);
        }
        return null;
    }
}
