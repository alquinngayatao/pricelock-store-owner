package com.pricelock.storeowner;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.pricelock.storeowner.model.ResetPasswordRequest;
import com.pricelock.storeowner.model.StoreOwnerRegisterRequest;
import com.pricelock.storeowner.utils.FileUtil;
import com.pricelock.storeowner.utils.HttpUtil;

import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PasswordReset extends AppCompatActivity {

    private Long id;
    Gson gsonObj = new Gson();
    Button btnResetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);

        Uri uri = getIntent().getData();

        // checking if the uri is null or not.
        if (uri != null) {
            // if the uri is not null then we are getting the
            // path segments and storing it in list.
            List<String> parameters = uri.getPathSegments();

            // after that we are extracting string from that parameters.
            String param = parameters.get(parameters.size() - 1);

            // on below line we are setting
            // that string to our text view
            // which we got as params.
            id = Long.parseLong(uri.getQueryParameter("id"));

        }

        btnResetPassword = findViewById(R.id.buttonSendRequest);
        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(PasswordReset.this, "Resetting password, please wait..", Toast.LENGTH_SHORT).show();
                new SendPasswordResetRequest().execute();
            }
        });
    }

    private class SendPasswordResetRequest extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Request request = new Request.Builder()
                    .url("http://192.168.51.246:8080/reset/password/store/owner")
                    .method("POST", RequestBody.create(gsonObj.toJson(ResetPasswordRequest.builder()
                                    .id(id)
                                    .password(((EditText) findViewById(R.id.editTextPassword)).getText().toString())
                            .build()), MediaType.parse("application/json")))
                    .build();

            try (Response response = HttpUtil.client.newCall(request).execute()) {
                Log.d("REGISTER_STOREOWNER", response.message());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            startActivity(new Intent(getApplicationContext(), PasswordResetSuccess.class));
            super.onPostExecute(aVoid);
        }
    }
}