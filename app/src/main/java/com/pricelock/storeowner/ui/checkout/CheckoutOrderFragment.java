package com.pricelock.storeowner.ui.checkout;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.pricelock.storeowner.databinding.FragmentCheckoutOrderBinding;

public class CheckoutOrderFragment extends Fragment {

    private FragmentCheckoutOrderBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        CheckoutOrderViewModel checkoutOrderViewModel =
                new ViewModelProvider(this).get(CheckoutOrderViewModel.class);

        binding = FragmentCheckoutOrderBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textSlideshow;
        checkoutOrderViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}