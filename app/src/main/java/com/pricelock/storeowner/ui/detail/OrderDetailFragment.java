package com.pricelock.storeowner.ui.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.pricelock.storeowner.databinding.FragmentOrderDetailBinding;

public class OrderDetailFragment extends Fragment {

    private FragmentOrderDetailBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        OrderDetailViewModel orderDetailViewModel =
                new ViewModelProvider(this).get(OrderDetailViewModel.class);

        binding = FragmentOrderDetailBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textGallery;
        orderDetailViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}