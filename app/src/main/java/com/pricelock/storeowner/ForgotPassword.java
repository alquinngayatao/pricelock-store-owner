package com.pricelock.storeowner;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.pricelock.storeowner.utils.HttpUtil;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

public class ForgotPassword extends AppCompatActivity {

    EditText emailAddressEditText;
    Button sendBtn;
    String emailAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        emailAddressEditText = (EditText) findViewById(R.id.editTextEmailAddressForgot);
        sendBtn = (Button) findViewById(R.id.buttonSendRequest);
        sendBtn.setOnClickListener(view -> {
            emailAddress = emailAddressEditText.getText().toString();
            Toast.makeText(ForgotPassword.this, "Sending forgot password request..", Toast.LENGTH_SHORT).show();
            new SendForgotPasswordRequest().execute();
        });
    }


    private class SendForgotPasswordRequest extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Request request = new Request.Builder()
                    .url("http://192.168.51.246:8080/forgot/password/store/owner?emailAddress="+emailAddress)
                    .build();

            try (Response response = HttpUtil.client.newCall(request).execute()) {
                Log.d("FORGOT_PASSWORD_STOREOWNER", response.message());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            startActivity(new Intent(getApplicationContext(), ForgotPasswordSuccess.class));
            super.onPostExecute(aVoid);
        }
    }
}