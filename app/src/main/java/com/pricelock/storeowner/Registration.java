package com.pricelock.storeowner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.codekidlabs.storagechooser.StorageChooser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pricelock.storeowner.model.BarangayRequest;
import com.pricelock.storeowner.model.CityRequest;
import com.pricelock.storeowner.model.Location;
import com.pricelock.storeowner.model.StoreOwnerRegisterRequest;
import com.pricelock.storeowner.utils.FileUtil;
import com.pricelock.storeowner.utils.HttpUtil;
import com.pricelock.storeowner.utils.LocationAdapter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Registration extends AppCompatActivity {
    Type listType = new TypeToken<ArrayList<Location>>() {
    }.getType();
    Context context;
    List<Location> provinces = new ArrayList<>();
    List<Location> cities = new ArrayList<>();
    List<Location> barangays = new ArrayList<>();
    Location selectedProvince;
    Location selectedCity;
    Location selectedBarangay;
    Gson gsonObj = new Gson();
    Spinner provinceSpinner;
    Spinner citySpinner;
    Spinner barangaySpinner;

    Button btnValidId;
    Button btnBrgyPermit;
    Button btnDTI;
    Button btnMayorPermit;
    ActivityResultLauncher<Intent> someActivityResultLauncher;
    TextView textViewTitleValidId;
    TextView textViewTitleBrgyBusinessPermit;
    TextView textViewTitleDTI;
    TextView textViewTitleMayorPermit;

    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        context = this;
        provinceSpinner = findViewById(R.id.spinnerProvince);
        citySpinner = findViewById(R.id.spinnerCity);
        barangaySpinner = findViewById(R.id.spinnerBarangay);
        new GetAllProvinceRequest().execute();

        btnValidId = findViewById(R.id.buttonValidId);
        btnBrgyPermit = findViewById(R.id.buttonBrgyBusinessPermit);
        btnDTI = findViewById(R.id.buttonDTI);
        btnMayorPermit = findViewById(R.id.buttonMayorPermit);

        someActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            // There are no request codes
                            Intent data = result.getData();
                            Toast.makeText(Registration.this, "Activity returned ok", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


        TextView dataPrivacyText = findViewById(R.id.textViewAgreement);
        dataPrivacyText.setOnClickListener(view -> {
            startActivity(new Intent(this, Agreement.class));
        });

        textViewTitleValidId = findViewById(R.id.textViewTitleValidId);
        textViewTitleBrgyBusinessPermit = findViewById(R.id.textViewTitleBrgyBusinessPermit);
        textViewTitleDTI = findViewById(R.id.textViewTitleDTI);
        textViewTitleMayorPermit = findViewById(R.id.textViewTitleMayorPermit);

        btnValidId.setOnClickListener(view -> pickFile(textViewTitleValidId));
        btnBrgyPermit.setOnClickListener(view -> pickFile(textViewTitleBrgyBusinessPermit));
        btnDTI.setOnClickListener(view -> pickFile(textViewTitleDTI));
        btnMayorPermit.setOnClickListener(view -> pickFile(textViewTitleMayorPermit));
        btnRegister = findViewById(R.id.buttonRegister);
        btnRegister.setOnClickListener(view -> {
            Toast.makeText(Registration.this, "Sending data please wait..", Toast.LENGTH_SHORT).show();
            new SendRegistrationRequest().execute();
        });
    }

    private void pickFile(TextView fileTextView) {
        // Initialize Builder
        StorageChooser chooser = new StorageChooser.Builder()
                .withActivity(Registration.this)
                .withFragmentManager(getFragmentManager())
                .withMemoryBar(true)
                .allowCustomPath(true)
                .setType(StorageChooser.FILE_PICKER)
                .build();

        // Show dialog whenever you want by
        chooser.show();

        // get path that the user has chosen
        chooser.setOnSelectListener(new StorageChooser.OnSelectListener() {
            @Override
            public void onSelect(String path) {
                fileTextView.setText(path);
            }
        });
    }

    private class SendRegistrationRequest extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Request request = new Request.Builder()
                    .url("http://192.168.51.246:8080/register/store/owner")
                    .method("POST", RequestBody.create(gsonObj.toJson(StoreOwnerRegisterRequest.builder()
                            .emailAddress(((EditText) findViewById(R.id.editTextEmailAddress)).getText().toString())
                            .username(((EditText) findViewById(R.id.editTextUsername)).getText().toString())
                            .password(((EditText) findViewById(R.id.editTextPassword)).getText().toString())
                            .phoneNumber(((EditText) findViewById(R.id.editTextNumber)).getText().toString())
                            .storeName(((EditText) findViewById(R.id.editTextStoreName)).getText().toString())
                            .province(selectedProvince.getLabel())
                            .city(selectedCity.getLabel())
                            .barangay(selectedBarangay.getLabel())
                            .streetAddress(((EditText) findViewById(R.id.editTextStreetAddress)).getText().toString())
                            .validId(FileUtil.convertToBase64(textViewTitleValidId.getText().toString()))
                            .barangayBusinessPermit(FileUtil.convertToBase64(textViewTitleBrgyBusinessPermit.getText().toString()))
                            .dtiRegistration(FileUtil.convertToBase64(textViewTitleDTI.getText().toString()))
                            .mayorPermit(FileUtil.convertToBase64(textViewTitleMayorPermit.getText().toString()))
                            .build()), MediaType.parse("application/json")))
                    .build();

            try (Response response = HttpUtil.client.newCall(request).execute()) {
                Log.d("REGISTER_STOREOWNER", response.message());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            LocationAdapter locationAdapter = new LocationAdapter(context, android.R.layout.simple_list_item_1, cities);
            citySpinner.setAdapter(locationAdapter);
            citySpinner.setSelection(locationAdapter.getCount());
            citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    startActivity(new Intent(getApplicationContext(), RegistrationSuccess.class));
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            super.onPostExecute(aVoid);
        }
    }

    private class GetAllProvinceRequest extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Request request = new Request.Builder()
                    .url("http://192.168.51.246:8080/location/province/all")
                    .build();

            try (Response response = HttpUtil.client.newCall(request).execute()) {
                provinces.addAll(gsonObj.fromJson(response.body().string(), listType));
                provinces.add(Location.builder()
                        .code("0")
                        .label("Province")
                        .build());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            LocationAdapter locationAdapter = new LocationAdapter(context, android.R.layout.simple_list_item_1, provinces);
            provinceSpinner.setAdapter(locationAdapter);
            provinceSpinner.setSelection(locationAdapter.getCount());
            provinceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    selectedProvince = (Location) provinceSpinner.getSelectedItem();
                    if (!selectedProvince.getCode().equals("0")) {
                        new GetAllCityByProvinceRequest().execute();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            super.onPostExecute(aVoid);
        }
    }

    private class GetAllCityByProvinceRequest extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Request request = new Request.Builder()
                    .url("http://192.168.51.246:8080/location/city")
                    .method("POST", RequestBody.create(gsonObj.toJson(CityRequest.builder()
                            .provinceCode(selectedProvince.getCode())
                            .build()), MediaType.parse("application/json")))
                    .build();

            try (Response response = HttpUtil.client.newCall(request).execute()) {
                cities.addAll(gsonObj.fromJson(response.body().string(), listType));
                cities.add(Location.builder()
                        .code("0")
                        .label("City")
                        .build());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            LocationAdapter locationAdapter = new LocationAdapter(context, android.R.layout.simple_list_item_1, cities);
            citySpinner.setAdapter(locationAdapter);
            citySpinner.setSelection(locationAdapter.getCount());
            citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    selectedCity = (Location) citySpinner.getSelectedItem();
                    if (!selectedCity.getCode().equals("0")) {
                        new GetAllBarangayByCity().execute();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            super.onPostExecute(aVoid);
        }
    }

    private class GetAllBarangayByCity extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Request request = new Request.Builder()
                    .url("http://192.168.51.246:8080/location/barangay")
                    .method("POST", RequestBody.create(gsonObj.toJson(BarangayRequest.builder()
                            .cityCode(selectedCity.getCode())
                            .build()), MediaType.parse("application/json")))
                    .build();

            try (Response response = HttpUtil.client.newCall(request).execute()) {
                barangays.addAll(gsonObj.fromJson(response.body().string(), listType));
                barangays.add(Location.builder()
                        .code("0")
                        .label("Barangay")
                        .build());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            LocationAdapter locationAdapter = new LocationAdapter(context, android.R.layout.simple_list_item_1, barangays);
            barangaySpinner.setAdapter(locationAdapter);
            barangaySpinner.setSelection(locationAdapter.getCount());
            barangaySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    selectedBarangay = (Location) barangaySpinner.getSelectedItem();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            super.onPostExecute(aVoid);
        }
    }

}